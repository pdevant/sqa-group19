package de.rwth.swc.sqa;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import static io.restassured.RestAssured.given;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;



@SpringBootTest(classes = MutableClockConfig.class, webEnvironment = RANDOM_PORT)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ValidateTicketTest {
    private static final String PATH = "/tickets/validate";
    private static long customerID = 0;

    private static long older_discount_card = 0;

    private static long newer_discount_card = 0;

    private static long discount_card_25 = 0;


    private List dc_ids = new LinkedList<Integer>();



    @LocalServerPort
    private int port;

    private long getFreeDiscountCardID(){
        for (long i = Long.MIN_VALUE; i < Long.MAX_VALUE; i++){
            if (!dc_ids.contains(i)){
                return i;
            }
        }
        throw new StackOverflowError();
    }


    @BeforeAll
    public void setUp() {
        RestAssured.port = port;

        // Dummy Customer
        Map<String, Object> body = new HashMap<>();
        body.put("birthdate", "1206-01-01");

        Response response = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(body)
                .when()
                .post("/customers")
                .then().extract().response();

        JsonPath jsonPath = new JsonPath(response.getBody().asString()) ;
        customerID = jsonPath.getLong("id") ;


        body = new HashMap<>();
        body.put("type", 50) ;
        body.put("validFrom", "1999-04-05") ;
        body.put("validFor", "1y") ;
        body.put("customerId", customerID);

        response = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(body)
                .when()
                .post(String.format("/customers/%s/discountcards", customerID))
                .then().extract().response();

        jsonPath = new JsonPath(response.getBody().asString());
        older_discount_card = jsonPath.getLong("id");


        body = new HashMap<>();
        body.put("type", 50) ;
        body.put("validFrom", "2021-12-05") ;
        body.put("validFor", "1y") ;
        body.put("customerId", customerID);

        response = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(body)
                .when()
                .post(String.format("/customers/%s/discountcards", customerID))
                .then().extract().response();

        jsonPath = new JsonPath(response.getBody().asString());
        newer_discount_card = jsonPath.getLong("id");

        body = new HashMap<>();
        body.put("type", 25) ;
        body.put("validFrom", "2023-12-05") ;
        body.put("validFor", "1y") ;
        body.put("customerId", customerID);

        response = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(body)
                .when()
                .post(String.format("/customers/%s/discountcards", customerID))
                .then().extract().response();

        jsonPath = new JsonPath(response.getBody().asString());
        discount_card_25 = jsonPath.getLong("id");

        dc_ids.add(older_discount_card);
        dc_ids.add(newer_discount_card);
        dc_ids.add(discount_card_25);

    }

    private static Stream<Arguments> valid_ticket_combinations(){
        List<Arguments> args = new LinkedList<>() ;
        Object[] discounts = new Object[]{null, 25, 50} ;

        for(int isStudent = -1 ; isStudent < 2 ; isStudent++){
            for(int isDisabled = -1 ; isDisabled < 2 ; isDisabled++){
                for(var discount : discounts){
                    if (isStudent == 1){
                        args.add(Arguments.of(isStudent, discount, isDisabled)) ;
                    }
                }
            }
        }
        return args.stream() ;
    }

    @ParameterizedTest
    @MethodSource("valid_ticket_combinations")
    void validation_ok(int isStudent, Integer discount, int isDisabled) {

        // Create ticket that will be tested
        HashMap body = new HashMap() ;
        if(isStudent != -1){
            body.put("student", isStudent != 0) ;
        }
        if(discount != null){
            body.put("discountCard", discount);
        }

        if(isDisabled != -1){
            body.put("disabled", isDisabled != 0) ;
        }
        body.put("birthdate", "2015-01-01") ;
        body.put("validFor", "30d") ;
        body.put("validFrom", "2022-05-05T10:38:59") ;

        var ticketResponse = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(body)
                .when()
                .post("/tickets")
                .then().extract().response();

        var jsonPath = new JsonPath(ticketResponse.getBody().asString()) ;
        var ticketID = jsonPath.getLong("id");


        // Validation Request
        body = new HashMap();
        if(isStudent != -1){
            body.put("student", isStudent != 0) ;
        }

        if(discount != null){
            body.put("discountCardId", newer_discount_card) ;
        }
        if(isDisabled != -1) {
            body.put("disabled", isDisabled != 0);
        }

        body.put("zone", "C");
        body.put("date", "2022-05-05T10:39:59");
        body.put("ticketId", ticketID);

        var response = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(body)
                .when()
                .post(PATH)
                .then();

        response.statusCode(200) ;
    }

    @Test
    void validation_1d_empty_zone() {

        // Create ticket that will be tested
        HashMap body = new HashMap() ;
        body.put("birthdate", "1998-01-01") ;
        body.put("validFor", "1d") ;
        body.put("validFrom", "2022-05-05T10:38:59") ;

        var ticketResponse = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(body)
                .when()
                .post("/tickets")
                .then().extract().response();

        var jsonPath = new JsonPath(ticketResponse.getBody().asString()) ;
        var ticketID = jsonPath.getLong("id");


        // Validation Request
        body = new HashMap();
        body.put("zone", "C");
        body.put("date", "2022-05-05T10:39:59");
        body.put("ticketId", ticketID);

        var response = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(body)
                .when()
                .post(PATH)
                .then();

        response.statusCode(200) ;
    }

    private static Stream<Arguments> valid_zone_generator(){
        List<Arguments> args = new LinkedList<>() ;
        args.add(Arguments.of("A","A")) ;

        args.add(Arguments.of("B","A")) ;
        args.add(Arguments.of("B","B")) ;

        args.add(Arguments.of("C","A")) ;
        args.add(Arguments.of("C","B"));
        args.add(Arguments.of("C","C")) ;

   ;

        return args.stream() ;
    }

    @ParameterizedTest
    @MethodSource("valid_zone_generator")
    void valid_zones(String ticket_zone, String valid_zone){
        // Create ticket that will be tested
        HashMap body = new HashMap() ;
        body.put("birthdate", "2015-01-01") ;
        body.put("validFor", "1h") ;
        body.put("zone", ticket_zone);
        body.put("validFrom", "2022-05-05T10:38:59") ;

        var ticketResponse = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(body)
                .when()
                .post("/tickets")
                .then().extract().response();

        var jsonPath = new JsonPath(ticketResponse.getBody().asString()) ;
        var ticketID = jsonPath.getLong("id");


        // Validation Request
        body = new HashMap();

        body.put("zone", valid_zone);
        body.put("date", "2022-05-05T10:38:59");
        body.put("ticketId", ticketID);

        var response = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(body)
                .when()
                .post(PATH)
                .then();

        response.statusCode(200) ;
    }

    private static Stream<Arguments> invalid_zone_generator(){
        List<Arguments> args = new LinkedList<>() ;
        args.add(Arguments.of("A","B")) ;
        args.add(Arguments.of("A","C")) ;
        args.add(Arguments.of("B","C")) ;

        return args.stream() ;
    }

    @ParameterizedTest
    @MethodSource("invalid_zone_generator")
    void invalid_zone(String ticket_zone, String valid_zone){
        // Create ticket that will be tested
        HashMap body = new HashMap() ;
        body.put("birthdate", "2015-01-01") ;
        body.put("validFor", "1h") ;
        body.put("zone", ticket_zone);
        body.put("validFrom", "2022-05-054T10:38:59") ;

        var ticketResponse = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(body)
                .when()
                .post("/tickets")
                .then().extract().response();

        Assertions.assertEquals(201, ticketResponse.statusCode());
        var jsonPath = new JsonPath(ticketResponse.getBody().asString()) ;
        var ticketID = jsonPath.getLong("id");


        // Validation Request
        body = new HashMap();

        body.put("zone", valid_zone);
        body.put("date", "2022-05-054T10:40:59");
        body.put("ticketId", ticketID);

        var response = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(body)
                .when()
                .post(PATH)
                .then();

        response.statusCode(403) ;
    }

    @Test
    void ticket_doesnt_exist(){
        // Validation Request
        var body = new HashMap();

        body.put("zone", "C");
        body.put("date", "2022-05-054T10:40:59");
        body.put("ticketId", -1);

        var response = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(body)
                .when()
                .post(PATH)
                .then();

        response.statusCode(400) ;
    }

    @Test
    void mismatch_student(){
        // Create ticket that will be tested
        HashMap body = new HashMap() ;

        body.put("student", false) ;
        body.put("birthdate", "2015-01-01") ;
        body.put("validFor", "30d") ;
        body.put("validFrom", "2022-05-05T10:38:59") ;

        var ticketResponse = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(body)
                .when()
                .post("/tickets")
                .then().extract().response();

        var jsonPath = new JsonPath(ticketResponse.getBody().asString()) ;
        var ticketID = jsonPath.getLong("id");


        body.put("student", true);
        body.put("zone", "C");
        body.put("date", "2022-05-05T10:39:59");
        body.put("ticketId", ticketID);

        var response = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(body)
                .when()
                .post(PATH)
                .then();

        response.statusCode(403) ;
    }


    @Test
    void mismatch_disabled(){
        // Create ticket that will be tested
        HashMap body = new HashMap() ;

        body.put("disabled", false) ;

        body.put("birthdate", "2015-01-01") ;
        body.put("validFor", "30d") ;
        body.put("validFrom", "2022-05-05T10:38:59") ;

        var ticketResponse = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(body)
                .when()
                .post("/tickets")
                .then().extract().response();

        var jsonPath = new JsonPath(ticketResponse.getBody().asString()) ;
        var ticketID = jsonPath.getLong("id");


        body.put("disabled", true);

        body.put("zone", "C");
        body.put("date", "2022-05-05T10:39:59");
        body.put("ticketId", ticketID);

        var response = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(body)
                .when()
                .post(PATH)
                .then();

        response.statusCode(403) ;
    }


    @ParameterizedTest
    @ValueSource(strings = {"2021-05-05T10:38:59","2023-05-05T10:38:59"})
    void ticket_date_fail(String ticket_date){
        // Create ticket that will be tested
        HashMap body = new HashMap() ;

        body.put("disabled", false) ;

        body.put("birthdate", "2015-01-01") ;
        body.put("validFor", "30d") ;
        body.put("validFrom", "2022-05-05T10:38:59") ;

        var ticketResponse = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(body)
                .when()
                .post("/tickets")
                .then().extract().response();

        var jsonPath = new JsonPath(ticketResponse.getBody().asString()) ;
        var ticketID = jsonPath.getLong("id");

        body.put("zone", "C");
        body.put("date", ticket_date);
        body.put("ticketId", ticketID);

        var response = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(body)
                .when()
                .post(PATH)
                .then();

        response.statusCode(403) ;
    }


    @Test
    void date_parse_error_is_400(){
        // Create ticket that will be tested
        HashMap body = new HashMap() ;

        body.put("disabled", false) ;

        body.put("birthdate", "2015-01-01") ;
        body.put("validFor", "30d") ;
        body.put("validFrom", "2022-05-05T10:38:59") ;

        var ticketResponse = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(body)
                .when()
                .post("/tickets")
                .then().extract().response();

        var jsonPath = new JsonPath(ticketResponse.getBody().asString()) ;
        var ticketID = jsonPath.getLong("id");

        body.put("zone", "C");
        body.put("date", "2022-05-05T25:38:60");
        body.put("ticketId", ticketID);

        var response = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(body)
                .when()
                .post(PATH)
                .then();

        response.statusCode(400) ;
    }



    @Test
    void missing_discount_card_date(){
        // Create ticket that will be tested
        HashMap body = new HashMap() ;

        body.put("birthdate", "2015-01-01") ;
        body.put("validFor", "30d") ;
        body.put("validFrom", "2022-05-05T10:38:59") ;
        body.put("discountCard", 50);

        var ticketResponse = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(body)
                .when()
                .post("/tickets")
                .then().extract().response();

        var jsonPath = new JsonPath(ticketResponse.getBody().asString()) ;
        var ticketID = jsonPath.getLong("id");

        body.put("zone", "C");
        body.put("date", "2022-05-05T10:40:59");
        body.put("ticketId", ticketID);

        var response = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(body)
                .when()
                .post(PATH)
                .then();

        response.statusCode(403) ;
    }

    @ParameterizedTest
    @ValueSource(strings = {"2021-12-04T10:40:59","2023-12-04T10:40:59"})
    void discount_card_date_fail(String date){
        // Create ticket that will be tested
        HashMap body = new HashMap() ;

        body.put("birthdate", "2015-01-01") ;
        body.put("validFor", "30d") ;
        body.put("validFrom", date);
        body.put("discountCard", 25);

        var ticketResponse = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(body)
                .when()
                .post("/tickets")
                .then().extract().response();

        var jsonPath = new JsonPath(ticketResponse.getBody().asString()) ;
        var ticketID = jsonPath.getLong("id");

        body.put("zone", "C");
        body.put("date", date);
        body.put("discountCardId", newer_discount_card);
        body.put("ticketId", ticketID);

        var response = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(body)
                .when()
                .post(PATH)
                .then();

        response.statusCode(403) ;
    }

    @Test
    void discount_card_not_existant(){
        // Create ticket that will be tested
        HashMap body = new HashMap() ;

        body.put("birthdate", "2015-01-01") ;
        body.put("validFor", "30d") ;
        body.put("validFrom", "2021-12-04T10:40:59");
        body.put("discountCard", 25);

        var ticketResponse = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(body)
                .when()
                .post("/tickets")
                .then().extract().response();

        var jsonPath = new JsonPath(ticketResponse.getBody().asString()) ;
        var ticketID = jsonPath.getLong("id");

        body.put("zone", "C");
        body.put("date", "2021-12-04T10:40:59");
        body.put("discountCardId", getFreeDiscountCardID());
        body.put("ticketId", ticketID);

        var response = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(body)
                .when()
                .post(PATH)
                .then();

        response.statusCode(400) ;
    }


    @Test
    void discount_50_but_card_25(){
        // Create ticket that will be tested
        HashMap body = new HashMap() ;

        body.put("birthdate", "2015-01-01") ;
        body.put("validFor", "30d") ;
        body.put("validFrom", "2023-12-05T10:40:59");
        body.put("discountCard", 50);

        var ticketResponse = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(body)
                .when()
                .post("/tickets")
                .then().extract().response();

        var jsonPath = new JsonPath(ticketResponse.getBody().asString()) ;
        var ticketID = jsonPath.getLong("id");

        body.put("zone", "C");
        body.put("date", "2023-12-05T10:40:59");
        body.put("discountCardId", discount_card_25);
        body.put("ticketId", ticketID);

        var response = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(body)
                .when()
                .post(PATH)
                .then();

        response.statusCode(403) ;
    }


}
