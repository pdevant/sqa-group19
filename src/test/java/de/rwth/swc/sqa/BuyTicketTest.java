package de.rwth.swc.sqa;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.ValidatableResponse;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

import static io.restassured.RestAssured.given;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;



@SpringBootTest(classes = MutableClockConfig.class, webEnvironment = RANDOM_PORT)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class BuyTicketTest {
    private static final String PATH = "/tickets";

    @LocalServerPort
    private int port;

    @BeforeAll
    public void setUp() {
        RestAssured.port = port;

    }


    HashMap<String, Object> getBody(){
        HashMap<String, Object> body = new HashMap() ;
        body.put("student", false) ;
        body.put("disabled", false) ;
        body.put("birthdate","2015-01-01") ;
        body.put("validFor", "30d") ;
        body.put("zone", "C");
        body.put("validFrom", "2020-10-29T10:38:59") ;

        return body ;
    }


    private static Stream<Arguments> valid_ticket_combinations(){
        List<Arguments> args = new LinkedList<>() ;
        String[] birthdays = new String[]{"2015-01-01", "2000-01-01", "1970-01-01", "1900-01-01"} ;
        Object[] discounts = new Object[]{null, 25, 50} ;

        for(int isDisabled = -1 ; isDisabled < 2 ; isDisabled++){
            for(var discount : discounts){
                for(String birth : birthdays){
                    args.add(Arguments.of(discount, isDisabled, birth)) ;

                }
            }
        }
        return args.stream() ;
    }


    @ParameterizedTest
    @MethodSource("valid_ticket_combinations")
    void valid_ticket(Integer discount, int isDisabled, String birthday) {
        HashMap<String, Object> body = new HashMap() ;


        body.put("discountCard", discount) ;

        if(isDisabled != -1){
            body.put("disabled", isDisabled != 0) ;
        }
        body.put("birthdate", birthday) ;
        body.put("validFor", "30d") ;
        body.put("validFrom", "2020-10-29T10:38:59") ;

        ValidatableResponse response = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(body)
                .when()
                .post(PATH)
                .then();

        response.statusCode(201) ;

    }

    @ParameterizedTest
    @ValueSource(strings = {"1y", "30d"})
    void invalid_zone_duration_combination(String dur){
        HashMap<String, Object> body = getBody() ;
        body.replace("zone","A") ;
        body.replace("validFor", dur) ;

        ValidatableResponse response = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(body)
                .when()
                .post(PATH)
                .then();

        response.statusCode(400) ;
    }


    private Stream<Arguments> invalid_student_combinations(){
        List<Arguments> args = new LinkedList<>() ;;

        // validFor invalid
        args.add(Arguments.of("1998-01-01", "1h", null));
        args.add(Arguments.of("1998-01-01", "1d", null));

        // birthday invalid
        args.add(Arguments.of("1985-01-01", "30d", null));
        args.add(Arguments.of("1970-01-01", "1y", null));

        // both invalid
        args.add(Arguments.of("1985-01-01", "1h", null));
        args.add(Arguments.of("1985-01-01", "1d", null));

        args.add(Arguments.of("2000-01-01", "30d", "A"));
        args.add(Arguments.of("2000-01-01", "30d", "B"));
        args.add(Arguments.of("2000-01-01", "30d", "C"));


        return args.stream() ;
    }

    @ParameterizedTest
    @MethodSource("invalid_student_combinations")
    void invalid_student_ticket(String birth, String validFor, String zone){
        HashMap<String, Object> body = getBody() ;

        body.replace("student", true);
        if (zone != null) {
            body.replace("zone", zone) ;
        }

        body.replace("birthday", birth);
        body.replace("validFor", validFor) ;

        ValidatableResponse response = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(body)
                .when()
                .post(PATH)
                .then();

        response.statusCode(400) ;
    }

    @ParameterizedTest
    @ValueSource(strings = {"1y", "30d"})
    void valid_student(String validFor){
        HashMap<String, Object> body = getBody() ;

        body.replace("student", true);
        body.replace("birthday", "1998-01-01");
        body.replace("validFor", validFor) ;
        body.replace("validFrom", "2020-10-29T10:38:59") ;
        body.remove("zone");

        ValidatableResponse response = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(body)
                .when()
                .post(PATH)
                .then();

        response.statusCode(201) ;
    }


    @Test
    void invalid_birthday(){
        HashMap<String, Object> body = getBody();
        body.replace("birthdate","1900-15-01") ;

        ValidatableResponse response = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(body)
                .when()
                .post(PATH)
                .then();

        response.statusCode(400) ;
    }


    private static Stream<Arguments> invalid_zones_combinations(){
        List<Arguments> args = new LinkedList<>() ;
        String[] timeZones = new String[]{"A", "B"};
        String[] duration = new String[]{"1d", "30d", "1y"} ;
        for(String time : timeZones){
            for(String dur : duration){
                args.add(Arguments.of(time,dur)) ;
            }
        }
        return args.stream() ;
    }


    @ParameterizedTest
    @MethodSource("invalid_zones_combinations")
    void invalid_zones(String zone, String duration){
        HashMap<String, Object> body = new HashMap() ;
        body.replace("zone", zone);
        body.replace("validFor", duration) ;


        ValidatableResponse response = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(body)
                .when()
                .post(PATH)
                .then();

        response.statusCode(400) ;
    }






}
