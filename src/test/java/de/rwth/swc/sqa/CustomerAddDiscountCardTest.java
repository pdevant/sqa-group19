package de.rwth.swc.sqa;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(classes = MutableClockConfig.class, webEnvironment = RANDOM_PORT)
class CustomerAddDiscountCardTest {
    private String PATH ;

    private String PATH_PARAM = "/customers/%s/discountcards";

    private long customerID ;

    @LocalServerPort
    private int port;

    @BeforeEach
    public void setUp() {

        RestAssured.port = port;

        Map<String, Object> body = new HashMap<>();
        body.put("birthdate", "1234-01-01");

        Response response = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(body)
                .when()
                .post("/customers")
                .then().statusCode(201).extract().response();

        JsonPath jsonPath = new JsonPath(response.getBody().asString()) ;

        customerID = jsonPath.getLong("id") ;

        PATH = String.format(PATH_PARAM, customerID);

    }

    @Test
    void discount_card_has_id_test() {
        Map<String, Object> body = new HashMap<>();
        body.put("type", 50) ;
        body.put("validFrom", "1999-04-05") ;
        body.put("validFor", "30d") ;
        body.put("customerId", customerID);
        body.put("id", 123) ;

        ValidatableResponse response = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(body)
                .when()
                .post(PATH)
                .then();

        response.statusCode(400) ;
    }

    @Test
    void discount_card_discount_customer_id_not_equal() {
        Map<String, Object> body = new HashMap<>();
        body.put("type", 50) ;
        body.put("validFrom", "1999-04-05") ;
        body.put("validFor", "30d") ;
        body.put("customerId", customerID+1);

        ValidatableResponse response = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(body)
                .when()
                .post(PATH)
                .then();

        response.statusCode(400) ;
    }

    @Test
    void discount_card_customer_id_unknown() {
        Map<String, Object> body = new HashMap<>();
        body.put("type", 50) ;
        body.put("validFrom", "1999-04-05") ;
        body.put("validFor", "30d") ;

        body.put("customerId", customerID+1);


        String newPath = String.format(PATH_PARAM, Long.toString(customerID+1));

        ValidatableResponse response = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(body)
                .when()
                .post(newPath)
                .then();

        response.statusCode(404) ;
    }

    @Test
    void discount_card_works() {
        Map<String, Object> body = new HashMap<>();
        body.put("type", 50) ;
        body.put("validFrom", "1999-04-05") ;
        body.put("validFor", "1y") ;
        body.put("customerId", customerID);

        ValidatableResponse response = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(body)
                .when()
                .post(PATH)
                .then();

        response.statusCode(201) ;

        Map<String, Object> body2 = new HashMap<>();
        body2.put("type", 50) ;
        body2.put("validFrom", "2003-04-05") ;
        body2.put("validFor", "30d") ;
        body2.put("customerId", customerID);

        ValidatableResponse response2 = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(body2)
                .when()
                .post(PATH)
                .then();

        response2.statusCode(201) ;
    }

    @Test
    void discount_card_bad_date() {
        Map<String, Object> body = new HashMap<>();
        body.put("type", 50) ;
        body.put("validFrom", "1999-02-31") ;
        body.put("validFor", "30d") ;
        body.put("customerId", customerID);

        ValidatableResponse response = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(body)
                .when()
                .post(PATH)
                .then();

        response.statusCode(400) ;
    }

    @Test
    void discount_card_bad_valid_for() {
        Map<String, Object> body = new HashMap<>();
        body.put("type", 50) ;
        body.put("validFrom", "1999-04-10") ;
        body.put("validFor", "100d") ;
        body.put("customerId", customerID);

        ValidatableResponse response = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(body)
                .when()
                .post(PATH)
                .then();

        response.statusCode(400) ;
    }

    @Test
    void discount_card_works_overlap() {
        Map<String, Object> body = new HashMap<>();
        body.put("type", 50) ;
        body.put("validFrom", "1999-04-05") ;
        body.put("validFor", "1y") ;
        body.put("customerId", customerID);

        ValidatableResponse response = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(body)
                .when()
                .post(PATH)
                .then();

        response.statusCode(201) ;

        Map<String, Object> body2 = new HashMap<>();
        body2.put("type", 50) ;
        body2.put("validFrom", "1999-07-05") ;
        body2.put("validFor", "30d") ;
        body2.put("customerId", customerID);

        ValidatableResponse response2 = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(body2)
                .when()
                .post(PATH)
                .then();

        response2.statusCode(409) ;
    }

}