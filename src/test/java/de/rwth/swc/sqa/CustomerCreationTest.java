package de.rwth.swc.sqa;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(classes = MutableClockConfig.class, webEnvironment = RANDOM_PORT)
class CustomerCreationTest {
    private static final String PATH = "/customers";

    @LocalServerPort
    private int port;
    @BeforeEach
    public void setUp() {
        RestAssured.port = port;
    }

    @Test
    void when_id_set_in_request() {
        Map<String, Object> body = new HashMap<>();
        body.put("birthdate", "1201-01-01");
        body.put("id", 1);

        Response response = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(body)
                .when()
                .post(PATH)
                .then()
                .extract().response();

        Assertions.assertEquals(400, response.statusCode());

    }

    @Test
    void when_is_valid() {
        Map<String, Object> body = new HashMap<>();
        body.put("birthdate", "1202-01-01");

        ValidatableResponse response = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(body)
                .when()
                .post(PATH)
                .then();

        response.statusCode(201).
                body(
                        "birthdate", is(body.get("birthdate")),
                        "id", notNullValue()
                );
    }

    @Test
    void invalid_date() {
        Map<String, Object> body = new HashMap<>();
        body.put("birthdate", "1203-01");

        ValidatableResponse response = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(body)
                .when()
                .post(PATH)
                .then();

        response.statusCode(400);
    }

    @Test
    void customer_disabled() {
        Map<String, Object> body = new HashMap<>();
        body.put("birthdate", "1204-01-01");
        body.put("disabled", true);

        ValidatableResponse response = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(body)
                .when()
                .post(PATH)
                .then();

        response.statusCode(201)
                .body(
                        "birthdate", is(body.get("birthdate")),
                        "disabled", is(true)
                );
    }

}