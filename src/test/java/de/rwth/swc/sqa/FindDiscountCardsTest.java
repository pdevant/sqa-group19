package de.rwth.swc.sqa;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;



@SpringBootTest(webEnvironment = RANDOM_PORT)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class FindDiscountCardsTest {
    private static final String PATH_CUSTOMERS = "/customers";
    private static final String PATH_DISCOUNT_CARDS = "/customers/%s/discountcards";

    private static Long customerWithCard = 0L;
    private static Long customerWithoutCard = 0L;

    @LocalServerPort
    private int port;

    @BeforeAll
    public void setUp() {
        RestAssured.port = port;
        Map<String, Object> body = new HashMap<>();
        body.put("birthdate", "1205-01-01");

        Response responseCustomer1 = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(body)
                .when()
                .post(PATH_CUSTOMERS)
                .then().statusCode(201).extract().response();

        Response responseCustomer2 = given().contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(body)
                .when()
                .post(PATH_CUSTOMERS)
                .then().statusCode(201).extract().response();

        JsonPath jsonPath = new JsonPath(responseCustomer1.getBody().asString());
        customerWithCard = jsonPath.getLong("id");

        jsonPath = new JsonPath(responseCustomer2.getBody().asString());
        customerWithoutCard = jsonPath.getLong("id");

        body = new HashMap<>();
        body.put("type", 50);
        body.put("validFrom", "1999-04-05");
        body.put("validFor", "1y");
        body.put("customerId", customerWithCard);

        ValidatableResponse responseCardCreated = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(body)
                .when()
                .post(String.format(PATH_DISCOUNT_CARDS, customerWithCard))
                .then();

        responseCardCreated.statusCode(201);


    }

    @ParameterizedTest
    @ValueSource(strings = {"", "xxx"})
    void invalid_customer_id(String customerID) {
        given().accept(ContentType.JSON).get(String.format(PATH_DISCOUNT_CARDS, customerID)).then().statusCode(400);
    }

    @Test
    void no_cards_found(){
        given().accept(ContentType.JSON).get(String.format(PATH_DISCOUNT_CARDS, customerWithoutCard)).then().statusCode(404);
    }

    @Test
    void customer_not_found(){
        given().accept(ContentType.JSON).get(String.format(PATH_DISCOUNT_CARDS, customerWithCard + customerWithoutCard + 1)).then().statusCode(404);
    }

    @Test
    void customer_with_cards(){
        Response response = given().accept(ContentType.JSON).get(String.format(PATH_DISCOUNT_CARDS, customerWithCard)).then().statusCode(200).extract().response();

        JsonPath jsonPath = new JsonPath(response.getBody().asString()) ;
        List x = jsonPath.getList("");


    }




}
