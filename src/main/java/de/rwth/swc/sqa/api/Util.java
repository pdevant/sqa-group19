package de.rwth.swc.sqa.api;

import java.time.LocalDateTime;

interface Util {

    static LocalDateTime calculateEndTime(LocalDateTime dt, String duration) {

        switch (duration) {
            case "1y":
                return dt.plusYears(1);
            case "30d":
                return dt.plusDays(30);
            case "1h":
                return dt.plusHours(1);
            case "1d":
                return dt.plusDays(1);
            default:
                //Only happens when there is a new discount duration which is not checked in the switch
                assert(false) ;
                return null;
        }
    }


    static boolean isDateBetween(LocalDateTime start, LocalDateTime end, LocalDateTime toCheck){
         return !(toCheck.isBefore(start) || toCheck.isAfter(end)) ;
    }

}
