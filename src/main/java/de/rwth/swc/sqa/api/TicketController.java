package de.rwth.swc.sqa.api;

import de.rwth.swc.sqa.model.DiscountCard;
import de.rwth.swc.sqa.model.Ticket;
import de.rwth.swc.sqa.model.TicketRequest;
import de.rwth.swc.sqa.model.TicketValidationRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Optional;

@Controller
public class TicketController implements TicketsApi {


    static HashMap<Long, Ticket> tickets = new HashMap<>();


    private boolean isValidTicketType(int ageInYears, boolean isStudent, TicketRequest.ValidForEnum validFor, TicketRequest.ZoneEnum zone) {
        if (validFor != TicketRequest.ValidForEnum._1H && zone != null){
            return false;
        }

        if (zone == null){
            zone = TicketRequest.ZoneEnum.C;
        }

        if (isStudent){
            if (zone != TicketRequest.ZoneEnum.C){
                return false;
            }

            if (ageInYears >= 28){
                // tries to use a student ticket but is over 28
                return false;
            }
            return validFor.equals(TicketRequest.ValidForEnum._30D) || validFor.equals(TicketRequest.ValidForEnum._1Y);

        }

        return true;

    }

    @Override
    public ResponseEntity<Ticket> buyTicket(TicketRequest body) {
        Ticket ticket = new Ticket();

        boolean isDisabled = false;
        boolean isStudent = false;

        if (body.getDisabled() != null) {
            isDisabled = body.getDisabled();
        }
        if (body.getStudent() != null) {
            isStudent = body.getStudent();
        }


        if (body.getDiscountCard() != null) {
            if (body.getDiscountCard() != 25 && body.getDiscountCard() != 50){
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
            ticket.setDiscountCard(body.getDiscountCard());
        }

        //filter out invalid tickets
        //first try to parse the given birthday string to get the age in years
        int ageInYears;
        try {

            LocalDate birthdate = LocalDate.parse(body.getBirthdate(), DateTimeFormatter.ISO_LOCAL_DATE);
            LocalDate today = LocalDate.now();
            ageInYears = Period.between(birthdate, today).getYears();

            if (Period.between(birthdate, today).getDays() < 0){
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }

        } catch (DateTimeException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        //Actual check if the ticket is valid
        if (!isValidTicketType(ageInYears, isStudent, body.getValidFor(), body.getZone())) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        //From here on the ticket must be valid
        long newTicketID = (long) tickets.size() +1 ;

        ticket.setId(newTicketID);
        ticket.setDisabled(isDisabled);
        ticket.setStudent(isStudent);
        ticket.setBirthdate(body.getBirthdate());
        ticket.setValidFrom(body.getValidFrom());
        ticket.setValidFor(Ticket.ValidForEnum.fromValue(body.getValidFor().toString()));
        if (body.getZone() != null){
            ticket.setZone(Ticket.ZoneEnum.fromValue(body.getZone().toString()));
        }

        tickets.put(newTicketID, ticket);

        return new ResponseEntity<>(ticket, HttpStatus.CREATED);
    }


    boolean checkZone(Ticket.ZoneEnum ticketZone, TicketValidationRequest.ZoneEnum requiredZone) {
        if (ticketZone == null){
            ticketZone = Ticket.ZoneEnum.C;
        }
        if (ticketZone == Ticket.ZoneEnum.C) {
            return true;
        } else if (ticketZone == Ticket.ZoneEnum.B) {
            return (requiredZone == TicketValidationRequest.ZoneEnum.A || requiredZone == TicketValidationRequest.ZoneEnum.B);
        } else {
            return requiredZone == TicketValidationRequest.ZoneEnum.A;
        }
    }


    public boolean validateBasicProperties(Ticket t, TicketValidationRequest body){
        if (body.getDisabled() == null){
            body.setDisabled(false);
        }

        if (body.getStudent() == null){
            body.setStudent(false);
        }

        // Check zone
        if (!checkZone(t.getZone(), body.getZone())) {
            return false;
        }

        if (!t.getDisabled().equals(body.getDisabled())) {
            return false;
        }

        // Student
        return t.getStudent().equals(body.getStudent());
    }

    @Override
    public ResponseEntity<Void> validateTicket(TicketValidationRequest body) {

        // Check if ticket exists
        if (!tickets.containsKey(body.getTicketId())) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        Ticket t = tickets.get(body.getTicketId());

        if (!validateBasicProperties(t, body)){
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }


        // Check date
        try {
            var ticketStartTime = LocalDateTime.parse(t.getValidFrom());
            var ticketEndTime = Util.calculateEndTime(ticketStartTime, t.getValidFor().toString());
            var validationTime = LocalDateTime.parse(body.getDate());

            if (!Util.isDateBetween(ticketStartTime, ticketEndTime, validationTime)) {
                return new ResponseEntity<>(HttpStatus.FORBIDDEN);
            }

            // Discount card
            if (t.getDiscountCard() != null) {
                if (body.getDiscountCardId() == null) {
                    return new ResponseEntity<>(HttpStatus.FORBIDDEN);
                }
                Optional<DiscountCard> discountCard = CustomerController.existsDiscountCard(body.getDiscountCardId());
                if (discountCard.isEmpty()) {
                    return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
                }

                if (discountCard.get().getType() < t.getDiscountCard()){
                    return new ResponseEntity<>(HttpStatus.FORBIDDEN);
                }

                var ticketValidationDate = LocalDateTime.parse(body.getDate());
                var discountCardStartDate = LocalDate.parse(discountCard.get().getValidFrom()).atStartOfDay();
                var discountCardEndDate = Util.calculateEndTime(discountCardStartDate, discountCard.get().getValidFor().toString());

                if (!Util.isDateBetween(discountCardStartDate, discountCardEndDate, ticketValidationDate)) {
                    return new ResponseEntity<>(HttpStatus.FORBIDDEN);
                }
            }

        } catch (DateTimeException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }


}
