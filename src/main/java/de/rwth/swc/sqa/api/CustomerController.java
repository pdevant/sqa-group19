package de.rwth.swc.sqa.api;

import de.rwth.swc.sqa.model.Customer;
import de.rwth.swc.sqa.model.DiscountCard;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Controller
public class CustomerController implements CustomersApi {

    static HashMap<Long, Customer> customers = new HashMap<>();

    static HashMap<Long, List<DiscountCard>> discounts = new HashMap<>();

    protected static Optional<DiscountCard> existsDiscountCard(long id){
        for (Map.Entry<Long, List<DiscountCard>> customer_discounts : discounts.entrySet()){
            for (DiscountCard card : customer_discounts.getValue()){
                if (card.getId() == id){
                    return  Optional.of(card) ;
                }
            }
        }
        return Optional.empty() ;
    }

    protected static long getNewCardId(){
        long size = 0;
        for (Map.Entry<Long, List<DiscountCard>> customer_discounts : discounts.entrySet()){
            size += customer_discounts.getValue().size();
        }
        return size+1;
    }


    @Override
    public ResponseEntity<Customer> addCustomer(Customer body) {

        if (body.getId() != null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        if (body.getDisabled() == null) {
            body.disabled(false);
        }

        try {
            LocalDate.parse(body.getBirthdate(), DateTimeFormatter.ISO_LOCAL_DATE);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }


        long newCustomerID = (long) customers.size() + 1 ;
        body.setId(newCustomerID);
        customers.put(newCustomerID, body);

        //append a fresh discount card list
        discounts.put(newCustomerID, new LinkedList<>()) ;

        return new ResponseEntity<>(body, HttpStatus.CREATED);
    }



    @Override
    public ResponseEntity<DiscountCard> addDiscountCardToCustomer(Long customerId, DiscountCard body) {

        if (body.getId() != null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        if (!customers.containsKey(customerId)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        if(!body.getCustomerId().equals(customerId)){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST) ;
        }

        LocalDateTime newTicketStartTime;
        LocalDateTime newTicketEndTime;
        try {
            newTicketStartTime = LocalDate.parse(body.getValidFrom()).atStartOfDay();
            newTicketEndTime = Util.calculateEndTime(newTicketStartTime, body.getValidFor().toString()) ;
        } catch (DateTimeException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        //check if discount card is in the time frame as another discount card of the same customer
        List<DiscountCard> currentDiscounts = discounts.get(customerId);

        for (DiscountCard loopDiscount : currentDiscounts) {

            LocalDate date = LocalDate.parse(loopDiscount.getValidFrom());
            LocalDateTime loopTicketStartTime = date.atStartOfDay();
            LocalDateTime loopTicketEndTime  = Util.calculateEndTime(loopTicketStartTime, loopDiscount.getValidFor().toString());

            if(!((newTicketStartTime.isAfter(loopTicketEndTime)  && newTicketEndTime.isAfter(loopTicketEndTime) )|| (newTicketStartTime.isBefore(loopTicketStartTime) && newTicketEndTime.isAfter(loopTicketEndTime)))){
                //Tickets overlap
                return new ResponseEntity<>(HttpStatus.CONFLICT) ;
            }
        }


        long newDiscountCardID = getNewCardId();
        body.setId(newDiscountCardID);
        discounts.get(customerId).add(body) ;

        return new ResponseEntity<>(body, HttpStatus.CREATED) ;

    }


    @Override
    public ResponseEntity<List<DiscountCard>> getCustomerDiscountCards(Long customerId) {

        if (!customers.containsKey(customerId)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        List<DiscountCard> customerCards = discounts.get(customerId);
        if (customerCards.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(customerCards, HttpStatus.OK);
        }
    }
}
